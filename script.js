const tasks = [
  { title: 'Faire les courses', isComplete: false },
  { title: 'Nettoyer la maison', isComplete: true },
  { title: 'Planter le jardin', isComplete: false }
];

const newTask = {
  title: "Faire le Live Coding",
  isComplete: false
}

const addTask = (taskList, taskToAdd) =>
  [...taskList, taskToAdd];

const newTaskList = addTask(tasks, newTask);
console.log("clone", newTaskList);

// suppresion des tâches complétées
const removeCompletedTask = (taskList) => {
  // quand on utilise map et filter, ça créé un nouveau tableau
  const taskFiltered = taskList.filter(
    (eachTask) => {
      // point exclamation !=
      if (eachTask.isComplete === true) {
        // valeur faux : je retire l'élément
        return false;
      } else {
        // valeur vrai : je garde l'élément
        return true;
      }
      // return !eachTask.isComplete
    }
  );
  return taskFiltered;
}

// const filterCompleted = (taskList) => taskList.filter((task) => !task.isComplete)
// const filterCompleted = (taskList) => taskList.filter((task) => task.isComplete ? false : true)

console.log("filter", removeCompletedTask(newTaskList));

const toggleTask = {
  title: "Inverser ma valeur",
  isComplete: false
};
const toggleTaskStatus = (task) => {
  if (task.isComplete === true) {
    task.isComplete = false;
  } else {
    task.isComplete = true;
  }
  return task;
}
// culture générale
const toggleTaskStatusExclamation = (task) => {
  // task.isComplete = task.isComplete === true ? false : true;
  const clone = {
    ...task,
    isComplete: !clone.isComplete
  }
  return clone;
}

console.log("toggle", toggleTaskStatus(toggleTask));

// filtre les tâches en fonction d'un état
const filterTasksByStatus = (taskList, status) => {
  /*
    status:
    - "completed"
    - "in progress"
    - "all"
  */
  const tasksFiltered = taskList.filter((eachTask) => {
    // status: "in progress"
    if (status === "in progress") {
      if (eachTask.isComplete === true) {
        return false;
      } else {
        return true;
      }
    } else if (status === "completed") {
      if (eachTask.isComplete === true) {
        return true;
      } else {
        return false;
      }
    } else {
      // "all"
      return true;
    }
  })

  return taskList.filter((eachTask) => {
    // status: "in progress"
    if (status === "in progress") {
      return !eachTask.isComplete
    } else if (status === "completed") {
      return eachTask.isComplete
    } else {
      return true;
    }
  })
}

console.log("filterTasksByStatus", filterTasksByStatus(tasks, "in progress"));

const junkFood = ["Tacos", "Burger", "Fries"];

console.log(junkFood);
console.log(...junkFood); // ce n'est plus un tableau, j'ai chaque valeurs séprarées par des virgules
console.log(...junkFood, "Kebab");

const newJunkFood = [...junkFood, "Kebab", "Pistaches"]
console.log(newJunkFood)

const cloneFood = [...junkFood];
cloneFood.push("Kebab")